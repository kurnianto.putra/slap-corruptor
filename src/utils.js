export const getTitle = (score) => {
  let title = "";
  if (score < 450) {
    title = "Slap Hunter";
  } else if (score < 500) {
    title = "Staff KPK";
  } else {
    title = "Corruptor's buddy";
  }
  return title;
};
export const uuid = `${new Date().getDate()}${new Date().getHours()}${new Date().getSeconds()}${new Date().getMilliseconds()}`;
