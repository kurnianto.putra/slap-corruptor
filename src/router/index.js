import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/",
    name: "Home",
    component: () => import("../components/Home.vue"),
  },
  {
    path: "/lboard",
    name: "LeaderBoard",
    component: () => import("../components/LeaderBoard.vue"),
  },
  {
    path: "/buyme",
    name: "BuyMe",
    component: () => import("../components/BuyMeCoffee.vue"),
  },
];

let router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
