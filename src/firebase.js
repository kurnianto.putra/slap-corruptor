import { onUnmounted, ref } from "vue";
import firebase from "firebase";
const config = {
  apiKey: "AIzaSyDZzBfARfTwoa7BFYWxUxbHhlLX1X1O4BE",
  authDomain: "slapsalp.firebaseapp.com",
  projectId: "slapsalp",
  storageBucket: "slapsalp.appspot.com",
  messagingSenderId: "1074821283675",
  appId: "1:1074821283675:web:e76e29a45abef92cdf647e",
  measurementId: "G-HLBM18W34N"
};

const firebaseApp = firebase.initializeApp(config);

const db = firebaseApp.firestore();
export const userCollection = db.collection("users");
export const commentCollection = db.collection("comments");
export const createRanking = (user) => {
  return userCollection.doc(user.name).set(user);
};
export const createComment = (user) => {
  return commentCollection.add(user);
};
export const updateRank = (id, user) => {
  return userCollection.doc(id).update(user);
};
export const getRankById = async (id) => {
  const rank = await userCollection.doc(id).get();
  return rank.exists ? rank.data() : null;
};

export const getAllRank = () => {
  const rank = {
    data: ref([]),
    loading: ref(true),
  };
  const close = userCollection
    .orderBy("score", "asc")
    .onSnapshot((snapshotChange) => {
      rank.data.value = snapshotChange.docs.map((doc) => ({
        ...doc.data(),
      }));
      rank.loading.value = false;
    });
  onUnmounted(close);
  return rank;
};

export const getAllComment = () => {
  const comments = {
    data: ref([]),
    loading: ref(true),
  };
  const close = commentCollection.onSnapshot((snapshotChange) => {
    comments.data.value = snapshotChange.docs.map((doc) => ({
      id: doc.id,
      ...doc.data(),
    }));
    comments.loading.value = false;
  });
  onUnmounted(close);
  return comments;
};
